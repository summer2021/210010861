use std::collections::HashMap;

fn tasks_check(doc: &yaml::Yaml) -> bool {
    match *doc {
        yaml::Yaml::String(ref h) => {
            let mut res = false;
            if h == "tasks" {
                res = true
            }
            res
        }
        _ => false
    }
}

fn name_check(doc: &yaml::Yaml) -> bool {
    match *doc {
        yaml::Yaml::String(ref h) => {
            let mut res = false;
            if h == "name" {
                res = true
            }
            res
        }
        _ => false
    }
}

fn denpens_on_check(doc: &yaml::Yaml) -> bool {
    match *doc {
        yaml::Yaml::String(ref h) => {
            let mut res = false;
            if h == "depends_on" {
                res = true
            }
            res
        }
        _ => false
    }
}

fn get_depends_on(doc: &yaml::Yaml){

    static mut root: HashMap<u32, Vec<&str>>;
    static mut point: HashMap<String, i32>;
    
    match *doc {
        yaml::Yaml::Array(ref h) => {
            for item in h {
                get_depends_on(item);
            }
        }
        yaml::Yaml::Hash(ref h) => {
            let mut name: bool = false;
            let mut depends_on: bool = false;
            
            for (k, _v) in h {
                if name_check(k) == true {
                    name = true;
                }
                if denpens_on_check(k) == true {
                    depends_on = true;
                }
            }

            if name == true {
                match h.get(&yaml::Yaml::String("name".to_string())) {
                    Some(value) => {
                        match value {
                            yaml::Yaml::String(ref name_str) => {
                                if depends_on == true {
                                    println!("[检查] >> {:?} 为子节点；", name_str);
                                    point.insert(name_str.to_string(), h.get(&yaml::Yaml::String("depends_on".to_string())));
                                }
                                else {
                                    println!("[检查] >> {:?} 为根节点；", name_str);
                                    root.insert(name_str.to_string(), "root".to_string());
                                }
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }
        }
        _ => {}
    }

    for root_item in root.iter() {
        println!("{:?}", root_item);
    }
}

fn dag_check(doc: &yaml::Yaml) -> bool {
    match *doc {
        yaml::Yaml::Hash(ref h) => {
            let mut res = false;
            for (k, v) in h {
                if tasks_check(k) == true {
                    get_depends_on(v);
                    res = true
                }
            }
            res
        }
        _ => false
    }
}
